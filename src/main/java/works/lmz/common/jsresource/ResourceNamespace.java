package works.lmz.common.jsresource;

import works.lmz.common.config.ConfigKey;
import works.lmz.common.stereotypes.SingletonBean;
import org.eclipse.jetty.util.StringUtil;

@SingletonBean
public class ResourceNamespace {

	public static final String DEFAULT_RESOURCE_NAMESPACE = "js";

	/**
	 * The global namespace of javascript resources
	 */
	@ConfigKey("lmz.namespace")
	protected String namespace = DEFAULT_RESOURCE_NAMESPACE;

	public String getNamespace() {
		if (StringUtil.isNotBlank(namespace)) {
			return namespace;
		}
		return DEFAULT_RESOURCE_NAMESPACE;
	}

}
